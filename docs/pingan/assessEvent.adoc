= 考核方案

== 状态图
[plantuml]
----
state "未发布" as new
state "已发布" as published
state "已完成" as finished
state "已终止" as terminated
new: s
[*] -> new: 创建
new -> published: 发布
new --> terminated: 关闭考核流程
published --> terminated: 关闭考核流程
published -> finished: 完成考核流程
----

[plantuml]
----
state "计划" as plan
state "汇报" as report
state "总结" as summary
state "排名组" as rankGroup
state "评等模型" as ratingModel
state "考核组" as assessGroup
state "考核方案" as assessEvent
state "考核流程" as assessProcess

assessProcess -> assessEvent
assessGroup -> assessEvent
assessEvent -> plan
plan -> report
report -> summary
----

[plantuml]
----
A -> B: can you <math>ax^2=0</math>
B -> A: <math>x = (-b+-sqrt(b^2-4ac))/(2a)</math>
----


asciimath:["总得分"=sum_() "考核工具得分" xx "权重" + sum_() "加减分"]
asciimath:["]

== 大模块
[plantuml]
----
class 考核方案
class 考核组
class 考核工具
class 考核计划
class 考核指标
class 考核链
class 考核链关联表
class 考核组考核人关联{
考核人
}

考核方案 --> 考核组
考核组考核人关联 --> 考核组
考核组 --> 考核工具
考核组 ..> 考核链
考核指标 --> 考核方案


----