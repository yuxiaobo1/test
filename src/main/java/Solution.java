public class Solution {

    static int[][] rights;
    static int[][] downs;

    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
            return 0;
        int max = 0;
        initRights(matrix);
        initDowns(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                max = find(i, j, max);
            }
        }

        return max;
    }

    static int find(int i, int j, int max) {
        int r = rights[i][j];
        int d = downs[i][j];
        if (r == 0) return max;

        int m;
        if (r > d) {
            m = r;
            int step = 1;
            while (d * r > max && step++ < d) {
                r = Math.min(r, rights[++i][j]);
                m = Math.max(m, step * r);
            }
        } else {
            m = d;
            int step = 1;
            while (d * r > max && step++ < r) {
                d = Math.min(d, downs[i][++j]);
                m = Math.max(m, step * d);
            }
        }
        return Math.max(max, m);
    }


    static void initRights(char[][] matrix) {
        rights = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            char[] line = matrix[i];

            int last = 0;
            for (int j = line.length - 1; j >= 0; j--) {
                last = line[j] == '0' ? 0 : last + 1;
                rights[i][j] = last;
            }
        }
    }

    static void initDowns(char[][] matrix) {
        downs = new int[matrix.length][matrix[0].length];
        for (int j = 0; j < matrix[0].length; j++) {
            int last = 0;
            for (int i = matrix.length - 1; i >= 0; i--) {
                last = matrix[i][j] == '0' ? 0 : last + 1;
                downs[i][j] = last;
            }
        }
    }

    public static void main(String[] args) {
//        char[][] input = {{'1', '0', '1', '0', '0'}, {'1', '0', '1', '1', '1'}, {'1', '1', '1', '1', '1'}, {'1', '0', '0', '1', '0'}};
//        char[][] input = {{'0'}};
//        char[][] input = {{'0', '0', '0', '0', '0', '0', '0', '1'}, {'0', '0', '0', '0', '0', '0', '0', '1'}, {'0', '0', '0', '0', '0', '0', '0', '1'}, {'0', '0', '0', '0', '0', '0', '0', '1'}, {'0', '0', '0', '0', '0', '0', '0', '1'}, {'0', '0', '0', '0', '0', '1', '1', '1'}, {'0', '0', '0', '0', '0', '1', '1', '1'}, {'1', '1', '1', '1', '1', '1', '1', '1'}};
        char[][] input = {{'0', '1', '0'}, {'1', '1', '1'}};
        int result = new Solution2().maximalRectangle(input);
        System.out.println(result);
    }
}