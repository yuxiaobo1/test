import java.util.Arrays;

/**
 * @author yuxiaobo
 */
public class MainTest {


}

class Solution2 {
    public int maximalRectangle(char[][] matrix) {
        if(matrix == null || matrix.length == 0)
            return 0;
        int n = matrix[0].length;
        int[] left = new int[n];
        int[] right = new int[n];
        int[] height = new int[n];

        Arrays.fill(right, n);
        int maxArea = 0;
        //iterate through and calculate area with dp matrix
        for(int i=0;i<matrix.length;i++)
        {
            int curr_right = n;
            int curr_left = 0;
            //height
            for(int j=0;j<n;j++)
            {
                if(matrix[i][j] == '1')
                {
                    height[j] = height[j]+1;
                }else
                {
                    height[j] = 0;
                }
            }
            //left boundary
            for(int j=0;j<n;j++)
            {
                if(matrix[i][j] == '1')
                {
                    left[j] = Math.max(left[j], curr_left);
                }else
                {
                    left[j] = 0;
                    curr_left= j+1;
                }
            }
            //right boundary
            for(int j=n-1;j>=0;j--)
            {
                if(matrix[i][j] == '1')
                {
                    right[j] = Math.min(right[j], curr_right);
                }else
                {
                    right[j] = n;
                    curr_right = j;
                }
            }
            for(int j=0;j<n;j++)
            {
                maxArea = Math.max(maxArea, (height[j] * (right[j] - left[j])));
            }
        }
        return maxArea;
    }
}