= 资金
:sectnums:
:sectnumlevels: 5

== 全局信息

- Project: app-box
- 库： db_association_adjust

== 群收（报价）

资金群收是资金的报价(offer)，主表为 `t_fund_account`， 属性列表如下：

[plantuml]
----
@startmindmap
* 资金报价\nFundOffer
** offerId
** 发布者
*** 发布者openId
*** 发布者机构Id
*** 显示名称(机构_姓名)
*** publisherType 类型： QQ QT
*** paymentType 付费类型: 付费、试用、未开通
** 接收者 \n FundOfferReceiver
*** 群号(群聊)
*** 接收者openId(私聊)
** 聊天记录id
** 报价主要属性
*** 方向
*** 期限
*** 量
*** 价格
*** 业务类型 \n FundBizModeEnum
**** 质押
**** 拆借
**** 买断
**** 协议回购
**** 任意
*** 评级 \n Rate
**** 评级类型 \n RateTypeEnum
***** 中债
***** 普通
***** 非中债
***** 隐含中债
**** 级别: AAA+ AA .. \n RateGradeEnum
*** 债券类型 11种\nFundBondTypeEnum
*** 资金要求类型 \n TagFundTypeEnum
*** 账户要求 \n AccountTypeEnum
*** 债券要求类型 \n BondRequireEnum
** hash
** ref: 是否失效
** expireTime: 过期时间
** source: 来源 \n InputSourceEnum
** fbBatchId: 群发id
@endmindmap
----

=== 属性和生成

==== offerId

顺序生成

==== 发布者

发布者的个人信息、机构信息、付费类型，用户类型 （QQ， QT）

==== 接收者

报价跟接收者是 `1对多` 的关系，每一个接收者用单独一行记录存储，表 `t_fund_offer_receiver`, 类 `FundOfferReceiver`

==== 报价来源

种类见 `InputSourceEnum`。

. 聊天记录： 所有聊天记录的入口都是 `rabbitMQ`, 接收消息的统一类是 `FundGroupMessageHandler`，详情见 `NLP解析`
. 手动解析、自动解析： 来源于 `群发` 功能
. FAKE： 群发功能如果不带有效报价，则会生成一条 offer， 来源为 `FAKE`，该报价只在 `我的发布` 中展示，其他地方不展示

==== 方向

`DirectionTypeEnum` , 1: 收， 2： 出

==== 期限

`FundGroupConvertUtil.convertPeriod` ， 支持将字符串转换成规范的期限， 多个期限用 `,` 隔开

[plantuml]
----
start
:以逗号隔开，转成数组;
:遍历;
:过滤掉在 periodIgnore 中的内容;
:替换每一项属于 fundPeriodReplace 中的内容;
:把每一项String 转成 List<NumberScope.Interval> ;
:收集所有 NumberScope.Interval， 转成 NumberScope;
stop
----

- 其中的 `periodIgnore` , `fundPeriodReplace` 内容可以在 `nacos` 配置中心查看和配置。

==== 量

`FundGroupConvertUtil.convertVolume` ， 支持将字符串转成规范的量。

[plantuml]
----
|convertVolume|
start
  :"接收字符串 text";
  :"简单替换，包括删除无效字符 和 单位统一";
|循环|
:"把 str 按 - 拆成前后2部分， 代表区间的 起-止";

|convertNumberUnit|
: 按 + 拆分字符串;
: 分别转成 VolumeNumberUnit;
: 相加，合并成单个 VolumeNumberUnit;
|循环|
:转换成 IntervalText;

|convertVolume|

:转换成 List<IntervalText>;
if (报价可拆?) then (yes)
 :插入一个 (0-MAX] 量;
else (no)
endif
:组装成 TextScopeValue ;
stop

----

==== 价格

直接从要素录入获取，或者从NLP 结果获取。

==== 债券类型 和 评级

- 债券类型和评级类 `BondLevelResult`， 包含 债券名称list， 债券代码list， 评级名称list， 评级编码list， 和展示的 tag
- 评级类 `Rate`，由两部分组成，评级类型 `RateTypeEnum` 和 评级级别 `RateGradeEnum`
- 解析工具类 `BondRateConvertUtil`

===== 债券解析

影响该内容的是NLP 解析结果中的3个字段

. bond_code
. bond_name
. bond_list

如果 `bond_code`, `bond_name` 有一个不为空， 则从中组合得到整体结果，否则从 `bond_list` 中获取结果。

==== 资金要求类型

TagFundTypeEnum

==== 账户要求

AccountTypeEnum

=== 去重

同一发布者同一天发布通过聊天记录发送的相同内容视为同一条报价，进行去重。

==== 条件

. 发布者相同
. 日期相同
. hash 相同
- hash 由报价原文转化而来，如果一行聊天内容解析成多条报价，则每条报价的原文hash拼接上序号。 见 FundOffer.staticGenHash
- 内容相同，具体见 FundOffer.replaceOriginString 注释

==== 去重处理细节

解析报价在存入数据库之前并不会判断是否有重复，而是统一调用方法 `FundGroupServiceImpl.processInsertOrUpdate`，进而调用 `processInsertOrUpdate` 持久化。`SQL` 语句为

[source]
----
insert into t_fund_offer values $(VALUES) on duplicate key update
Fpublish_time=GREATEST(Fpublish_time, #{item.publishTime}), modified_time=now(), Fref=#{item.ref}, f_source=#{item.source},
        Fgroup_code=#{item.groupCode}
----

因为 t_fund_offer 表存在唯一索引

- 如果不存在重复报价，则作为新报价插入
- 如果存在重复数据，则执行 `on duplicate key update` 后面的语句，更新部分字段的值

最后不管是否新报价，都将接收对象存入 `t_fund_offer_receiver`， 改表同样设置了唯一索引，不过 SQL 采用了 `insert ignore` 语句，自动忽略掉重复数据，减少不必要的业务代码判断。

=== 群发

群发分两阶段

. 报价预览: 不管是哪种预览方式，都将返回统一的 FundOffer 格式内容
.. 要素报价预览
.. 解析报价预览
. 消息群发： 将预览阶段得到的 List<FundOffer> 作为报价进行群发

API 入口类 `FundMassMessagingController`

==== 要素报价预览

方法 `elementParsePreview`， 将要素组装成为格式化的 `FundOffer`

==== 文本解析预览

方法 `textParsePreview`，将文本解析成为 `List<FundOffer>`

==== 消息群发

方法 `massMessaging`。

[plantuml]
----
|handleMassMessaging|
start
:获取用户信息;
|validateAndInitialize|
:校验token;
:消息不能为空;
if (不带报价) then (yes)
 :生成一条source为FAKE的报价;
else (no)
endif
:规范所有报价，重置hash，校验报价有效性;
if (用户设置了报价的过期时间 ? ) then (yes)
 :校验过期时间有效性;
else (no)
 :设置默认过期时间(下一个 18:00);
endif
:设置所有报价的过期时间;
|handleMassMessaging|
:格式化聊天消息input.getFinalMessage();
:校验消息长度是否合理;
:将输入的各种类型的通讯录解析成统一格式\nUmsDirectlyProxy.parseContact();
:校验发送对象大小在配置的范围内(1000);
:校验额度是否充足;
:调用UMS接口发送消息 sendMassMessage();
:保存发送结果 saveResult();
if (发送后的异步处理 afterSend()) then
    stop
else
|afterSend|
 -[#blue,dotted]->
:获取发送成功的对象列表;
:调用dubbo服务保存\n saveErpReport();
:调用dubbo服务扣减额度\n decreaseQuota();
:根据所有发送对象\n生成相应数量的offers;
:批量持久化offer\nbatchAddOffer();
:推送通知给前端\nsendNotification();
stop

----

===== 群发的解析问题

群发消息不进行解析，而是将客户端传入的 `offerList` 作为报价进行存储。对于发出去的聊天内容，统一在消息末尾拼接特殊字符串 `\u009C\u009C`，聊天记录发现内容以此特殊字符结尾的时候不进行解析。

注意： 此字符串不只是在资金使用，也在其他多个业务使用，拼接上此字符串之后可能导致其他业务也不对消息进行处理。见 `GlobalChatUtil.MSG_INVISIBLE_STR`

==== 重发

方法 `resend`, 对于某一次群发(batchId) 结果有失败对象的，针对失败对象进行重发。以下情况不能调用这个接口重发:

. 发送结果全部成功
. 有发送失败对象，但所有失败对象都是不可重发类型，包括
- 发送对象不是有效用户
- 不是好友关系
- 其他 `企点` 接口认为失败的场景

重发主流程

. 前端传入上次成功的 batchId
. 后端在 `MassMessagingServiceImpl.resent` 方法中组织群发参数
. 将上一步产生的参数传入 之前的 群发功能 `handleMassMessaging`

MassMessagingServiceImpl.resent 过程

[plantuml]
----
start
:从数据库表 t_fund_batch_send 中查出 batchId 对应的记录 record;
:校验 record 为今日数据;
:校验 record 发送者为当前用户;
:校验 record 包含有效可重发对象;
:在 t_fund_offer 表中查出 batchId 关联的所有报价;
:组装 MassMessagingInput 记录作为参数;
:将 MassMessagingInput 传入 handleMassMessaging 进行重发;
----

=== 历史数据

`fund_offer` 和 `fund_offer_receiver` 只存30天内数据，超出日期的分别存入 `fund_offer_history` 和 `fund_offer_receiver_history`。过程见 `FundGroupBackupJob`

注意： 修改上面两张主表结构的时候需要同时修改两张备份表，否则会导致备份出错。

== 晴雨表

晴雨表是表示每个开通资金功能的用户，按天统计收到的资金报价汇总。所有数据存在 `redis` 中，不存 `MySQL`。

=== 数据结构

==== OFFERS

记录每个用户每一天接收到的所有报价，当天报价实时更新，每个用户每天一个期限类型 1条数据

. key 结构 DATA:APPBOX:FUND:BAROMETER:OFFERS:{DATE}:{OPEN_ID}:{PERIOD}
- DATE: 日期 yyyy-MM-dd
- OPEN_ID: 用户openId
- PERIOD: 期限， 1：隔夜， 7： 7D
. 数据类型： zSet
- value: offerId
- score: 时间戳
. TTL： 7D

==== MAX_VOLUME

记录每条报价的最大量，如果报价的量被修改，可以实时更新，每天一条数据

. key 结构： DATA:APPBOX:FUND:BAROMETER:MAX_VOLUME:{DATE}
. 数据类型： hash
- hash key: offerId
- hash value: 量， 正代表出， 负代表收， 正负 39571 是特殊字符，代表0， 见 `DirectionVolumeConverter.numToSave`
. TTL: 7D

==== HISTORY

用户过往每天收到的报价笔数和量总和的统计，每个用户每种期限类型 1条数据

. key结构: DATA:APPBOX:FUND:BAROMETER:HISTORY:{OPEN_ID}:{PERIOD}
. 数据类型 hash
- hash key: 日期, yyyy-MM-dd
- hash value: 量和笔数的字符拼接，见 `Barometer.ser()`

=== 数据的实时更新

`FundBarometerService` 实现了 接口 `FundOfferEventHandler`， 当有新的报价变动时，调用 `onFundOfferEvent` 方法。

==== 插入或更新报价

FundBarometerService.addOrUpdateOffer

[plantuml]
----
start
:确保报价发布时间在有效时间段内;
:获取报价的最大量;
if (UPDATE 或者 没写如果 maxVolume?) then (yes)
 :写入maxVolume 到redis 和 cache;
else (no)
endif
:遍历所有接收者，将报价写入接收者 的 OFFERS 记录;
stop
----

由于redis key 设计的唯一性， 重复操作并不会产生错误数据。

==== 删除报价

- 方法： FundBarometerService.deleteOffer
- 将 MAX_VOLUME 相关的值删除

=== 查看数据

==== 单日统计

- 方法: FundBarometerController.getInstance

==== 曲线图

- 方法： FundBarometerController.getLine

==== 历史统计

- 方法： FundBarometerController.getHistory

== 账户、头寸、意向

=== 类图

[plantuml]
----
class FundAccount{
int id
String name
String orgId
}
class FundPosition{
int id
LocalDate date
String orgId
int accountId
String accountName
int direction
int periodStart
int periodEnd
BigDecimal volume
BigDecimal usedVolume
int intentCount
}
class FundIntent{
int id
String publisherOrgId
String publisherRealName
String publisherQtNumber
int accountId
String accountName
int direction
String counterparty
String periodText
int periodNum
String volumeText
int volumeNum
String priceText
int priceNum
}

FundPosition "many" -left-> "1" FundAccount
FundIntent "many" -left-> "1" FundPosition
----

=== 账户 FundAccount

机构可以配置多个账户，机构名下的账户名字是唯一的，有头寸权限的用户都可以配置账户。

=== 头寸 FundPosition

每个机构内部共享头寸数据，主要要素为

. accountId 账户 id
. date 日期
. direction 方向
. periodStart - periodEnd 期限的区间范围

在 accountId， date， direction 3个要素确定的情况下，period 范围是不能重复的。

- 例如3要素相同时，有一个头寸的期限范围是 1-10D, 那么再创建一个期限为10D 的头寸会报错

==== 头寸 scope

判断头寸的重叠与否是通过 头寸的scope 来判断的， 辅助表为 `t_fund_account_daily_position_scope`，该表字段为：

. f_account_id: 账户id
. f_date: 日期
. f_direction: 方向
. f_scope: 期限， 字符串， 格式为 : `[1-10),[11-23)`
- 多项之间用 `,` 隔开
- 每一项为一个区间，包含第一个数字且不包含最后一个数字的 `半开半闭` 区间
- 单个数字也用区间表示： `[7-8)` 表示 `7`

对 `scope` 的修改操作在 类 `FundPositionScopeService` 中，提供了3个方法

. removeScope: 从原有scope 中删除一个区间，可能会导致区间的拆分
. appendScope: 向scope 插入一个区间， 可能会产生区间的合并

=== 意向 FundIntent

`意向` 要素类似资金群收 `报价`，主要区别是

- 意向 只有本方可以看到，不需要通知交易对手
- 意向数据本机构的人都可以看到，叫 `意向汇总`
- 意向包含交易对手字段
- 意向需要包含账户信息，而且一条意向必须属于某个头寸，如果没有与之对应的头寸，则自动创建

=== 消息推送

FundPublishService.publishOrg 方法发送账户、头寸、意向 的变动通知，通知整个机构所有成员。

==== Future

这里面主要有个参数需要注意： `@Nullable Future<Boolean> sendFuture`

[plantuml]
----
start
if(sendFuture == null ) then (yes)
 :send;
 stop
else (no)
 :boolean b = sendFuture.get(timeout);
 if (timeout? ) then (yes)
stop
 elseif (b?) then (yes)
      :send;
        stop
 else
stop
----

传递 future 的主要原因是，有些大事务，前面的任务成功了，后面的任务失败，那么前面需要等整个事务的结果之后才能判断是否发送消息。

== NLP解析

=== NLP 文档

http://212.129.164.73:9106/showdoc/web/#/10?page_id=501

=== 本地解析代码

. 统一调用方法： FundGroupParseService.parse
. FeignClient: FundGroupTextNlpRepo.parsing
. NLP 请求拦截器: FundGroupParseInterceptor， 所有解析请求都需要经过 FundGroupParseInterceptor 的拦截

=== 解析入口

. 聊天记录解析 FundGroupMessageHandler.parse
. 群发报价预览 MassMessagingServiceImpl.parse
. 手动意向解析 FundIntentService.parseOnly
. 手动头寸解析 FundPositionService.parsePosition

=== 解析结果类型

. 资金报价 FundOffer
. 资金头寸 FundPosition
. 资金意向 FundIntent