= 权限
:sectnums:
:sectnumlevels: 5

. 项目结构： https://gitlab.qtrade.com.cn/dev/dev_backend_sz/qtrade-privilege-provider

- common-privilege: 公共依赖的API，提供接口类
- base-privilege: dubbo 服务，接口实现

. 数据库: db_apple_tree
. 相关负责人：
- 初版： 蔡毅勇
- 再版： 余晓波
- 后续: 多人修改，具体看 git 提交记录

== 权限信息

- 表: t_privilege_info
- 实体类: PrivilegeInfo

=== 主要字段

- id
- name 权限名称
- order 权限码
- level 级别，1： 顶级， 直接下级比直接上级 +1
- type 权限类型： 1：（个人+机构) , 2: 仅机构有效， 3： 仅个人有效
- sys_code 没用
- url 没用
- expand_yn 是否有机构表头
- data_yn 是否有机构子权限
- user_data_yn 是否有用户子权限
- privilege_from 用户权限值在哪张表,
.. 1: t_bond_module_privilege_account
.. 2: t_bond_business_privilege_account
- yn 没用
- old_privilege_order 老的用户权限表（privilege_from）中的 order 值
- old_privilege_sub_order 老的用户权限表（privilege_from）中的 sub_order 值


== 机构权限

- 表: t_privilege_org

=== 主要字段

- id
- company 机构的 node id
- order 权限码，对应表 t_privilege_info 中的 order 字段
- yn 没用

=== 操作

对机构权限的操作入口集中在 BasePrivilegeService 类中

==== 获取机构权限

. 方法： BasePrivilegeService.getOrgPrivilege，
. 流程： 递归从 t_privilege_org 表中查出每一层的权限数据
. 返回结果： 权限树，可能包含没有权限的内容，需要判断权限值的 selected 字段
. 问题： 如果机构的权限层级较多，会出现多次数据库的查询操作

==== 新增或修改机构权限

. 方法： saveOrgPrivilege，这是一个替换操作，保存之前会把旧数据清空
. 主要流程: OrgPrivilegeService.saveOrgPrivileges

[plantuml]
----
start
if (清除权限 ) then (no)
  :检查机构支付状态在有效期内;
endif
:找出保存之前的旧权限(IO);
:查找要保存权限的所有子权限树(IO);
:对比新老权限的差异 SaveOrgChangedData(不需IO);
:插入新权限;
:删除机构旧权限;
:删除机构下个人旧权限;
:刷新表头模板数据;
stop
----

==== 删除机构权限

. 方法： deleteUserPrivilegeByOrg
. 说明：
- 初版之后没变动
- 很可能没有用到

== 用户权限

在做这个权限系统之前，有2个表记录着个人权限，分别是

. t_bond_module_privilege_account
. t_bond_business_privilege_account .

为了兼容，权限系统保存用户权限的时候也是保存到这两张表中，具体是哪一张表，取决于 `privilege_info` 表中的字段 `privilege_from`

.新老个人权限关系映射
|===
|privilege_from |old table |old_privilege_order |old_privilege_sub_order

|1
|t_bond_module_privilege_account
|老表 order 字段
|老表 sub_order 字段

|2
|t_bond_business_privilege_account
|老表 order 字段
|老表 privilege_code 字段
|===

=== 主要字段

权限系统改造后，2张旧的权限表都加入了以下字段：

. privilege_order: 新的权限码，对应表 t_privilege_info 中的 order 字段
. company: 机构的 node id.

权限系统对个人权限的操作都是根据 open_id 字段 和 privilege_order 来选择数据， 而原有的系统则根据原有的权限字段(order + sub_order) 或者 （order + privilege_code）

=== 操作

主要入口： BasePrivilegeService 类

==== 获取个人权限

. 方法： getUserPrivilege
. 过程： 递归按层返回数据， 如果个人有某权限 `p` 的记录，但是机构没有 `p` 的权限记录，则认为个人没有 `p` 权限
. 返回结果： 权限树，可能包含没有权限的内容，需要判断权限值的 selected 字段
. 注意： 递归查找数据库，可能导致效率低下

==== 保存权限

. 方法: saveUserPrivilege
. 主要流程： IndividualPrivilegeService.savePrivileges

[plantuml]
----
start
:查找用户的旧权限;
:查找需要修改的权限树;
:对比生成权限差异结果 changedData;
if ( 取消所有权限 ?) then (no)
 :校验机构的支付状态;
endif
:删除减少的权限;
:插入新增的权限;
stop
----

=== 用户权限变更通知和确认

. 接口 PrivilegeChangeService
. 实体类 PrivilegeChangeConfirm
. 表 t_privilege_change_confirm
. 功能
- 当用户权限发生变更时，会插入（或更新）一条记录，客户端可以通过接口查询到这些变更的权限。
- 客户端可以对这些变更进行 `确认` 操作，确认之后客户端不会返回这些变更内容。

==== 主要字段
. open_id(key) 用户 open id
. privilege_order(key) 权限编码
. privilege_value(key) 权限值类型， 0:取消权限， 1: 开启权限, 2: 离职
. org_id 机构id， 查询的时候可以带上机构id， 如果用户所属的机构变更，可以快速忽略掉旧机构的数据
. state 状态, 1: 已更新， 2: 已确认

==== 操作

- getOpenIdsWhichDisabledNotConfirm : 列取所有取消了权限但是未确认的记录
- getOpenIdsWitchDisabledNotConfirm :  获取取消权限且未确认的 openId
- getOpenIdsWhichNotConfirm : 根据权限值获取没有确认的权限变更, 例如想要获取未确认的离职权限变更，传入 {@link PrivilegeValueEnum#LEAVE}
- getNotConfirmByAncestorPrivilege: 根据上级权限，查找需要通知权限变更列表, 这个接口一个人查出来可能有多条记录, 比如某个人原来有  1002-1 和 1002-2，离职后分别产生一条离职权限变更记录，用 ancestorPrivilege = 1002 查出来之后两条数据都有
- confirmChangedOffspringPrivileges: 批量确认后代权限变化的通知，包括所有权限变更类型(启用，停用，离职)
- confirmChangedOffspringPrivileges: 批量确认后代权限变化的通知，只更改一种变更类型
- getAnyOffspringChangedByOpenIdAndAncestorOrder: 根据 openId 和 祖先节点 查看后代权限是否有变更, 比如输入 ancestorOrder = 1001-1 则 1001-1 下面的任何权限变更都会返回true， 没有任何变更则返回false

== 机构支付管理

表 t_erp_org_payment_state，记录机构对于某产品的付费状态和到期情况。

=== 主要字段

. org_id 机构 node id
. product_code 产品编码， 见 PaymentProductEnum
. org_name 机构名称（冗余）
. org_type 机构类型(冗余)，见 db_org_user.t_organization_node_info.FType
. org_state 机构状态（冗余）， db_org_user.t_organization_node_info.FOrg_state
. expire_time 有效期的过期时间
. quota 配额，之前的需求是该配额只对付费记录有效（必须 >0），试用机构无效（无限)
. product_detail 部分产品有二级产品的选购，用 JSON key-value 格式保存
. department_id 机构的部门id（冗余），见 db_org_user.t_organization_node_info.FDepartment_id
- 部分产品需要过滤掉 department_id 为空的记录

=== 操作

主要方法入口在 ErpOrgPaymentStateService

==== 插入或更新记录

updatePayment -> updatePaymentOption

. 采用强制更新的方法 `replace into`，如果没有数据则插入新数据，如果存在数据则替换为新数据。
. 部分功能需要给机构初始化权限

=== 其他操作

- getAll: 获取所有记录
- listAllOrgIds: 根据 orgId 找出所有记录
- listForExcludeTrialRequest: 列出不能申请试用的机构id,
- getByOrgId: 查询单个
- pageByQuery: 带参数的分页请求
- trialExtension: 试用延长期限

